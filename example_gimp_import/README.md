# Gimp import

Requirements:

* [TexturePacker](https://www.codeandweb.com/texturepacker)
* GIMP with [ExportLayers plugin](https://github.com/khalim19/gimp-plugin-export-layers/releases/tag/3.3.1)

Skin Manager runs `export.bat` file in location of imported .xcf file and then proceeds to import ready spritesheet from `exported` folder in same location (just as it would import .json in first place). Everything else can be changed to match your personal needs.

This example is from my project. What is worth noting:

* Each folder is separate skin project.
* Name of project is matching name of folder and that is matching imported id in skin manager
* `export.bat` starts `export_base.bat` with folder name as argument
* In `export_base.bat` you might need to update variables on top of file to match your locations of gimp and TexturePacker
* I .gitignore exported files, they are not necessary as their copy will end in content folder anyway
