﻿using IniParser.Model;
using MonoPanda.UtilityClasses;

namespace SkinManager {
  public class SkinManagerConfig : DefaultIniSectionLoader {

    public readonly string ProjectConfigFile;
    public readonly string StartProjectPath;
    public readonly string StartSpineProjectId;
    public readonly string EnableSkins;
    public readonly string AssetsPath;

    public SkinManagerConfig(IniData iniData) : base(iniData, "SkinManager") {
    }
  }
}