﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonoPanda.UI {
  public interface ISelectionItem {
    string GetLabel();
  }
}
