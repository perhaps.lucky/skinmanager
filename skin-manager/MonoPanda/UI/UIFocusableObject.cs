﻿namespace MonoPanda.UI {
  public abstract class UIFocusableObject : UIObject, IFocus {
    public bool Focused {
      get => isFocused();
      set {
        if (value)
          OnFocused();
        else
          OnUnfocused();

        if (IFocus.Focus != null && IFocus.Focus != this && IFocus.Focus is UIFocusableObject) {
          ((UIFocusableObject)IFocus.Focus).OnUnfocused();
        }

        IFocus.Focus = value ? this : null;
      }
    }

    public bool Active { get; set; } = true;

    private bool isFocused() {
      return this == IFocus.Focus;
    }

    public virtual void OnFocused() { }

    public virtual void OnUnfocused() { }
  }
}