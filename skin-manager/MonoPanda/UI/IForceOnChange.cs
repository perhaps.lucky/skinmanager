﻿namespace MonoPanda.UI {
  public interface IForceOnChange {
    void ForceOnChange();
  }
}
