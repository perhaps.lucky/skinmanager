﻿using Microsoft.Xna.Framework.Media;
using MonoPanda.BitmapFonts;
using MonoPanda.Configuration;
using MonoPanda.Logger;
using MonoPanda.Multithreading;
using MonoPanda.Sound;
using MonoPanda.Spine.MonoPanda;
using MonoPanda.SpriteSheets;
using MonoPanda.Tiled.Content;
using MonoPanda.Timers;
using MonoPanda.Utils;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

using SkinManager;

using System;

namespace MonoPanda.Content {
  public class ContentItem {

    public string Id { get; set; }
    
    [JsonConverter(typeof(StringEnumConverter))]
    [JsonIgnore]
    public ContentItemType ItemType { get; set; }
    [JsonProperty(NullValueHandling=NullValueHandling.Ignore)]
    public string FilePath { get; set; } // path inside mod folder
    [JsonProperty(NullValueHandling=NullValueHandling.Ignore)]
    public string AtlasPath { get; set; }
    [JsonProperty(NullValueHandling=NullValueHandling.Ignore)]
    public string JsonPath { get; set; }
    [JsonProperty(NullValueHandling=NullValueHandling.Ignore)]
    public string TexturePath { get; set; }
    [JsonProperty(NullValueHandling=NullValueHandling.Ignore)]
    public string Type { get; set; }
    [JsonProperty(NullValueHandling=NullValueHandling.Ignore)]
    public string ExternalSkinsPath { get; set; }
    

    [JsonIgnore]
    public string FullPath { get; set; } // includes mod folder from which it should be loaded
    public bool NeverUnload { get; set; }
    
    [JsonIgnore]
    public bool IsLoaded => item != null;

    private object item;
    
    private SingleTimeTimer unloadTimer;
    private ContentRequest request;
    private StreamHolder holder;

    public T getItem<T>(bool forceLoad = false, bool ignoreTimer = false) {
      if (!IsLoaded) {
        if (forceLoad)
          return getForceLoad<T>();

        if (!getRequest().InQueue)
          ContentLoader.PutRequest(getRequest());

        return default;
      }

      if(!ignoreTimer)
        resetUnloadTimer();
      return (T)item;
    }

    public void load() {
      if (IsLoaded)
        return;

      switch (Type) {
        case "Sprite":
          if (holder == null)
            holder = new StreamHolder(FullPath);
          item = ContentUtils.LoadTexture(holder);
          break;
        // case ContentItemType.SoundEffect:
        //   item = ContentUtils.LoadSoundEffect(FullPath);
        //   break;
        // case ContentItemType.Song:
        //   item = Song.FromUri(Id, new Uri(FullPath, UriKind.Relative));
        //   break;
        // case ContentItemType.BitmapFont:
        //   item = new BitmapFont(FullPath);
        //   break;
        case "SpriteSheet":
          if (holder == null)
            holder = new StreamHolder($"{FullPath}{FilePath}");
          item = new SpriteSheet(holder);
          break;
        case "SpineAsset":
          item = new SpineAsset(FullPath, FilePath, Project.GetWorkingExternalSkins());
          break;
        // case ContentItemType.TiledMap:
        //   if (holder == null)
        //     holder = new StreamHolder(FullPath);
        //   item = new TiledMapContentItem(holder);
        //   break;
      }
      Log.log(LogCategory.Content, LogLevel.Info, "ContentItem id: " + Id + " loaded successfully.");
      resetUnloadTimer();
    }

    public void checkUnload() {
      if (unloadTimer != null && unloadTimer.Check())
        unload();
    }

    public void ReloadSpine() {
      
    }

    private T getForceLoad<T>() {
      load();
      return (T)item;
    }

    private ContentRequest getRequest() {
      if (request == null)
        request = new ContentRequest(Id);
      return request;
    }

    private void resetUnloadTimer() {
      if (Config.Content.ContentUnloadActive) { // this check effectively blocks entire unloading - timer will never exist so it won't be checked
        if (unloadTimer == null) {
          unloadTimer = new SingleTimeTimer((int)Config.Content.ContentUnloadTime, "Content_" + Id);
        }

        unloadTimer.Initialize(true);
      }
    }

    public void unload() {
      if (NeverUnload || !IsLoaded)
        return;

      Log.log(LogCategory.Content, LogLevel.Debug, "Attempting unloading item id: " + Id );
      if (ItemType == ContentItemType.Song && Id.Equals(SoundManager.GetCurrentlyPlayedSongId())) {
        Log.log(LogCategory.Content, LogLevel.Debug, "Item id: " + Id + " not unloaded because it's currently playing song.");
        resetUnloadTimer();
        return;
      }

      if (item is IDisposable)
        ((IDisposable)item).Dispose();
      item = null;
      Log.log(LogCategory.Content, LogLevel.Info, "Item id: " + Id + " unloaded.");
      GC.Collect();
    }

  }
}
