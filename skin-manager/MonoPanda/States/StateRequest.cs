﻿using MonoPanda.Multithreading;

namespace MonoPanda.States {
  public class StateRequest : ThreadRequest {
    public State State { get; private set; }
    public bool Reinitialize { get; private set; }

    public StateRequest(State state, bool reinitialize) {
      this.State = state;
      this.Reinitialize = reinitialize;
    }
  }
}