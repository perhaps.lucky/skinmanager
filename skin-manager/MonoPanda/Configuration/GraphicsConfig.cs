﻿using IniParser.Model;
using MonoPanda.UtilityClasses;

namespace MonoPanda.Configuration {
  public class GraphicsConfig : DefaultIniSectionLoader {

    public readonly bool IsFixedTimeStep;
    public readonly int TargetElapsedTimeMillis;
    public readonly bool SynchronizeWithVerticalRetrace;

    public GraphicsConfig(IniData iniData) : base(iniData, "Graphics") { }
  }
}