﻿using MonoPanda.Multithreading;
using MonoPanda.Tiled.Json;
using MonoPanda.Utils;

namespace MonoPanda.Tiled.Content {
  public class TiledMapContentItem {
    public TiledMap TiledMap { get; set; }
    
    public TiledMapContentItem(StreamHolder holder) {
      TiledMap = ContentUtils.LoadJson<TiledMap>(holder.Path);
    }
  }
}