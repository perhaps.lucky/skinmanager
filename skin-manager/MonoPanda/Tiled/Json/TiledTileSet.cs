﻿using System.Collections.Generic;

namespace MonoPanda.Tiled.Json {
  public class TiledTileSet {
    public int FirstGid { get; set; }

    public int TileCount { get; set; }

    public int TileHeight { get; set; }

    public int TileWidth { get; set; }

    public string Name { get; set; }

    public int Columns { get; set; } 
    
    public List<TiledTile> Tiles { get; set; }

    public TiledTile FindTileByGID(int GID) {
      foreach (var tile in Tiles) {
        if (GID - FirstGid == tile.Id)
          return tile;
      }

      return null;
    }
  }
}