﻿namespace MonoPanda.Tiled.Json {
  public class TiledTile {
    public int Id { get; set; }

    public string Image { get; set; }

    public int ImageHeight { get; set; }

    public int ImageWidth { get; set; }
    
    public string Type { get; set; }
  }
}