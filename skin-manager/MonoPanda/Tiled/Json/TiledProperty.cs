﻿namespace MonoPanda.Tiled.Json {
  public class TiledProperty {
    public string Name { get; set; }
    public string Type { get; set; }
    public object Value { get; set; }
  }
}