﻿using MonoPanda.ECS;

using System.Collections.Generic;

namespace MonoPanda.Tiled.Json {
  public class TiledMap : TiledClassWithProperties {

    public Dictionary<int, Entity> IdToEntity { get; set; } = new Dictionary<int, Entity>();
    
    public int Height { get; set; }

    public int Width { get; set; }

    public List<TiledLayer> Layers { get; set; }

    public string Orientation { get; set; }

    public string RenderOrder { get; set; }

    public int TileHeight { get; set; }

    public int TileWidth { get; set; }
    
    public List<TiledTileSet> Tilesets { get; set; }

    public List<TiledProperty> Properties { get; set; }

    protected override List<TiledProperty> getPropertyList() {
      return Properties;
    }

    protected override TiledMap getMap() {
      return this;
    }
  }
}