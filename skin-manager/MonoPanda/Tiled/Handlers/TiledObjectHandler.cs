﻿using Microsoft.Xna.Framework;

using MonoPanda.ECS;
using MonoPanda.Tiled.Json;

using System;

namespace MonoPanda.Tiled.Handlers {
  public class TiledObjectHandler {
    private Func<Vector2, TiledObject, TiledLayer, Entity> create;
    private Action<Entity, TiledObject, TiledLayer> postInit;

    public TiledObjectHandler(Func<Vector2, TiledObject, TiledLayer, Entity> create = null,
      Action<Entity, TiledObject, TiledLayer> postInit = null) {
      this.create = create;
      this.postInit = postInit;
    }

    public Entity HandleCreate(Vector2 position, TiledObject o, TiledLayer layer) {
      if (create != null)
        return create.Invoke(position, o, layer);
      return null;
    }

    public void HandlePostInit(Entity entity, TiledObject o, TiledLayer layer) {
      if (postInit != null)
        postInit.Invoke(entity, o, layer);
    }
  }
}