﻿using MonoPanda.Components;
using MonoPanda.ECS;
using MonoPanda.ECS.Components.DrawingComponent;
using System;

namespace MonoPanda.Systems {
  public class SpriteAnimationSystem : EntitySystem {
    public SpriteAnimationSystem(string id, int updateInterval = -1) : base(id, updateInterval) {
    }

    public override void Update() {
      foreach (SpriteSheetAnimationComponent component in ECS.GetAllComponents<SpriteSheetAnimationComponent>()) {
        if (componentQualifiesForUpdate(component)) {
          if (hasLastFrameEnded(component)) {
            handleAnimationEnded(component);
          } else {
            component.CurrentFrame++;
          }

          component.UpdateSpriteSheetSprite();
        }
      }
    }

    private bool componentQualifiesForUpdate(SpriteSheetAnimationComponent component) {
      return
        !component.AnimationFinished
        && component.CurrentAnimation != null
        && component.FrameTimer.Check(true)
        && component.Entity.IsInVisibleArea;
    }

    private void handleAnimationEnded(SpriteSheetAnimationComponent component) {
      if (component.CurrentAnimation.IsReversing) {
        reverseAnimation(component.CurrentAnimation);
        component.CurrentFrame = 1; // 2nd frame because first just got played
      } else if (component.CurrentAnimation.IsLooping) {
        component.CurrentFrame = 0;
      } else {
        component.StopAnimation();
      }
    }

    private void reverseAnimation(SpriteSheetAnimation animation) {
      Array.Reverse(animation.Frames);
      animation.isReversed = !animation.isReversed;
    }

    private bool hasLastFrameEnded(SpriteSheetAnimationComponent component) {
      return component.CurrentFrame == component.CurrentAnimation.Frames.Length - 1;
    }

    public override string ToString() {
      return "%{MediumSpringGreen}% SpriteAnimationSystem";
    }
  }
}
