﻿using Microsoft.Xna.Framework;

namespace MonoPanda.ECS.Systems.Camera {
  public struct CameraState {
    public Vector2 Position { get; set; }
    public float Zoom { get; set; }

    public CameraState(Vector2 position, float zoom) {
      Position = position;
      Zoom = zoom;
    }
  }
}
