﻿using Microsoft.Xna.Framework;
using MonoPanda.Components;
using MonoPanda.Configuration;
using MonoPanda.Utils;

namespace MonoPanda.ECS {
  /// <summary>
  /// This is class for inner helper updates. It should not be used outside.
  /// </summary>
  public class InnerEntityUpdateSystem {
    private EntityComponentSystem ecs;

    public InnerEntityUpdateSystem(EntityComponentSystem entityComponentSystem) {
      ecs = entityComponentSystem;
    }

    public void update() {
      Rectangle visibleArea = ecs.Camera.GetVisibleArea();

      foreach (Entity entity in ecs.GetAllEntities()) {
        entity.IsInVisibleArea = checkInVisibleArea(entity, visibleArea);
      }
    }

    private bool checkInVisibleArea(Entity entity, Rectangle visibleArea) {
      DrawComponent drawComponent = entity.GetComponent<DrawComponent>();

      if (drawComponent == null)
        return checkInVisibleAreaNoComponent(entity, visibleArea);
      else
        return checkInVisibleAreaDrawComponent(drawComponent, visibleArea);
    }

    private bool checkInVisibleAreaNoComponent(Entity entity, Rectangle visibleArea) {
      if (Config.Debug.VisibilityRectangles) {
        DrawUtils.DrawRectangle(ecs, new Rectangle(entity.Position.ToPoint(), new Point(1)), Color.Cyan);
      }
      return visibleArea.Contains(entity.Position.ToPoint());
    }

    private bool checkInVisibleAreaDrawComponent(DrawComponent component, Rectangle visibleArea) {
      if (component.Size == Vector2.Zero)
        return true;

      Vector2 position = component.Entity.Position - getOrigin(component) * getScale(component);
      Rectangle componentRectangle = new Rectangle(position.ToPoint(), (component.Size * getScale(component)).ToPoint());
      if (getRotation(component) != 0) {
        componentRectangle = RectangleUtils.ApplyRotation(componentRectangle, getRotation(component), component.Entity.Position);
      }

      if (Config.Debug.VisibilityRectangles) {
        DrawUtils.DrawRectangle(ecs, componentRectangle, new Color(245,188,0,70));
      }

      return visibleArea.Intersects(componentRectangle);
    }

    private Vector2 getOrigin(DrawComponent component) {
      if (component.HasSetOrigin)
        return component.Origin;

      return component.Size / 2f;
    }

    private float getScale(DrawComponent component) {
      return component.Scale * component.Entity.Scale;
    }

    private float getRotation(DrawComponent component) {
      return component.Rotation + component.Entity.Rotation;
    }
  }
}
