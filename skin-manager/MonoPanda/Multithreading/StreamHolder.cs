﻿using System;
using System.IO;

namespace MonoPanda.Multithreading {
  public class StreamHolder : IDisposable {

    public string Path { get; private set; }

    private FileStream fileStream;
    private int users;

    public StreamHolder(string path) {
      Path = path;
    }

    public FileStream Open() {
      lock(this) {
        if (users == 0)
          fileStream = new FileStream(Path, FileMode.Open);

        users++;
        return fileStream;
      }
    }

    public void Close() {
      lock (this) {
        users--;

        if (users == 0)
          fileStream.Close();
      }
    }

    public void Dispose() {
      Close();
    }
  }
}
