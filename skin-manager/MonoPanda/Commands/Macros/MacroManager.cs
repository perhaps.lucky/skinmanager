﻿using MonoPanda.Configuration;
using MonoPanda.Utils;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace MonoPanda.Commands.Macros {
  public class MacroManager {
    private static Dictionary<string, string> macros;

    public static void Initialize() {
      if (File.Exists(Config.Console.UserMacrosJson))
        macros = ContentUtils.LoadJson<Dictionary<string, string>>(Config.Console.UserMacrosJson);
      else
        macros = new Dictionary<string, string>();
    }

    public static bool IsInitialized() {
      return macros != null;
    }

    /// <summary>
    /// Returns `true` if overrided already existing macro.
    /// </summary>
    public static bool AddMacro(string name, string command) {
      if (macros.ContainsKey(name)) {
        macros[name] = command;
        save();
        return true;
      } else {
        macros.Add(name, command);
        save();
        return false;
      }
    }

    public static void RemoveMacro(string name) {
      macros.Remove(name);
      save();
    }

    public static Optional<string> GetMacro(string name) {
      if (macros.ContainsKey(name))
        return Optional<string>.Of(macros[name]);
      else
        return Optional<string>.Empty();
    }

    public static Dictionary<string, string> GetMacros() {
      return macros;
    }

    private static void save() {
      File.WriteAllText(Config.Console.UserMacrosJson, JsonConvert.SerializeObject(macros, Formatting.Indented));
    }
  }
}
