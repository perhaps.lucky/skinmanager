﻿using IniParser.Model;
using MonoPanda.Logger;
using System;
using System.Globalization;
using System.Reflection;

namespace MonoPanda.UtilityClasses {
  public abstract class DefaultIniSectionLoader {

    public DefaultIniSectionLoader(IniData iniData, string sectionName) {
      if (iniData[sectionName] == null) {
        Log.log(LogCategory.Ini, LogLevel.Warn, "Section: " + sectionName + " is missing in ini data. Values will not be loaded.");
        return; // section is missing in file
      }

      foreach (KeyData keyData in iniData.Sections.GetSectionData(sectionName).Keys) {
        FieldInfo fieldInfo = this.GetType().GetField(keyData.KeyName);
        if (fieldInfo != null) {
          fieldInfo.SetValue(this, Convert.ChangeType(keyData.Value, fieldInfo.FieldType, CultureInfo.InvariantCulture));
          continue;
        }

        PropertyInfo propertyInfo = this.GetType().GetProperty(keyData.KeyName);
        if (propertyInfo != null) {
          propertyInfo.SetValue(this, Convert.ChangeType(keyData.Value, propertyInfo.PropertyType));
          continue;
        }

        Log.log(LogCategory.Ini, LogLevel.Warn, "Field/property named: " + keyData.KeyName + " has not been found. Property will not be loaded into config objects.");
      }
    }

  }
}
